package opigrupp1;

public class Answer3 {

	   public static void main (String[] args) {
	      System.out.println (keskmisestParemaid (new double[]{1, -4, 5, -2, 6, 4}));
	      // YOUR TESTS HERE
	   }

	   public static int keskmisestParemaid (double[] d) {
		  double keskmine = 0; //elementide keskmine väärtus
		  double elementideSumma = 0; // kõigi elementide summa
		  int elementideArv = 0; // elementide keskmisest väärtusest suuremate elementide arv
		  
		  //leian massiivi elementide aritmeetilise keskmise
		  // kõigepealt nende summa
		  // iga massiivi d elemendi "element" kohta tee seda:
		  for(int i = 0; i < d.length; i++){    		//for(double element : d)
			  //elementideSumma = elementideSumma + d[i];
			  elementideSumma += d[i];					// elementideSumma += element;
		  }
		  
		  //leian keskmise
		  keskmine = elementideSumma / d.length;
	      
		  // leian elementide arvu, mis on suuremad aritmeetilisest keskmisest
		  for(double element : d){
			  if(element > keskmine){
				  elementideArv++;
			  }
		  }
		 
		  return elementideArv;  // YOUR PROGRAM HERE
	   }

	}
