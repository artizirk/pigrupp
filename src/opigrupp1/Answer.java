package opigrupp1;

public class Answer {

	   public static void main (String[] args) {
		   
	      System.out.println (ruutudeSumma (5., -5.));
	      System.out.println(summaRuut(-3, 8));
	      System.out.println(summaAbsoluutV22rtus(-3, 5));
	      System.out.println(absoluutv22rtusteSumma(-5, 3));
	      // YOUR TESTS HERE
	   }
	   public static double summaRuut(double a, double b){
		   // liidab a+b ning võtab seejärel teise astmesse
		   return Math.pow(a+b, 2);
	   }

	   public static double ruutudeSumma (double a, double b) {
		   // leiab nii a kui ka b asboluutväärtused, seejärel liidab need
	      return Math.pow(a, 2) + Math.pow(b,  2);
	   }
	   
	   public static double summaAbsoluutV22rtus(double a, double b){
		   //liidab a ja b, seejärel leiab nende absoluutväärtuse
		   return Math.abs(a + b);
	   }
	   
	   public static double absoluutv22rtusteSumma(double a, double b){
		   //leiab a ja b absoluutväärtused, seejärel liidab need
		   return Math.abs(a) + Math.abs(b);
	   }
}
