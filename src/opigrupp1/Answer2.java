package opigrupp1;

public class Answer2 {

	   public static void main (String[] args) {
	      System.out.println (negSumma (new int[]{-2, 0, 4, -3, 5, -4}));
	      // YOUR TESTS HERE
	   }
	   
	   //leiab etteantud massiivi negatiivsete elementide summa
	   public static int negSumma (int[] m) {
	      int summa = 0;
	      for(int element : m){
	    	  if(element < 0){
	    		  summa += element;
	    	  }
	      }     
	      return summa;
	   }
	 //leiab etteantud massiivi positiivsete elementide summa
	   public static int posSumma (int[] m) {
	      int summa = 0;
	      for(int element : m){
	    	  if(element > 0){
	    		  summa += element;
	    	  }
	      }     
	      return summa;
	   }
	   

	}
