package opigrupp1;

public class Meetotid /* see on nimi */ {

	private static int tereTere() {
		return 1;
	}
	
	public static int veelTere(int saadetis){
		saadetis = saadetis + 1;
		return saadetis;
	}
	
	public static void main(String[] args) {
		int mingimuutuja = tereTere();
		int veelMuutuja = veelTere(5);
		System.out.println(mingimuutuja);
		System.out.println(veelMuutuja);
	}
	

}
