package opigrupp1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Muutujad {

	public static void main(String[] args) {
		
		int number = 42;  // see ja alumine on samad
		
		int number2;
		number2 = 666;
		// -0.5e2
		
		double komakohaga = 5.6;
		boolean tingimus = false;
		char minumuutuja = 'a';
		
		String minuTekst = "Tere maailm!";
		
		int a = 5;
		double b = 2;
		double c = a / b;
		//System.out.printf("%d", c);  // %d prindib int muutujaid
		System.out.printf("%3.3f", c);  // %f on komakohaga (float, double)
		
		System.out.print("tere :");
		System.out.print(a);  // 
		System.out.println(" oli number");
		
		System.out.println("tere :" + a + " oli number");
		
		System.out.printf("tere :%d oli number%n", a);
		System.out.printf("int: %d, double: %f, String: %s", a,c,minuTekst);
		//teksti printime kasutades printf
		String tekstiMuutuja = "see on tekst";
		System.out.printf("\n minu tekst: %s ", tekstiMuutuja);
		//täisarvu printimine kasutades printf
		int miskiNumber = 3;
		System.out.printf("\n see on number: %d", miskiNumber);
		// komakohaga arvu printimine kasutades printf
		double komaKohaga = 3.56565;
		System.out.printf("\n see on komakohaga arv: %f ", komaKohaga);
		
		
		
		
		
		int[] veelmassiive;
		int[] massiiv = {1, 2, 3, 99};
		//int[] j2rgminemassiiv = new int[] {1,3,4};
		int[][] kaksDMassiiv = {
								{1, 2},
								{3, 4},
								{5, 6}
							   };
		
		//System.out.println(kaksDMassiiv[0][1]);  //prints 2
		
		String[] minuLaheTekst = new String[5];
		
		// lisame 1. sahti teksti
		minuLaheTekst[0] = "Kass";
		
		// lisame 2. sahti teksti
		minuLaheTekst[1] = "Tere Koer!";
		
		// see ei toimi sest 42 ei ole String tüüp!
		//minuLaheTekst[2] = 42;
		
		
		massiiv[0] = 42; // esimese sahtli väärtus on 42
		massiiv[3] = 5;
		massiiv[2] = 5*5; //kolmanda sahtli väärtus on 5 korda 5
		
		
		
		
		//System.out.println(massiiv[0] + " " + massiiv[3]);
		
		int[] kahegajaguvad = new int[100];
		
		for(int i = 0; i < 100; i++){
			kahegajaguvad[i] = i;
		}
		
		String minuTest = "salakala";
		
		//System.out.println(minuTest.length());
		
		Scanner input = new Scanner(System.in);
		//System.out.print("input: ");
		//int s = input.nextInt();
		int s = getInt();
		System.out.println(s);
		
		
	}
	
	public static int getInt() {
		int asdf;
		for (asdf = -1; asdf != 2;) {
			try {
				Scanner input = new Scanner(System.in);
				System.out.print("input: ");
				asdf = input.nextInt();
				//break;  // exit while
			} catch(InputMismatchException e)  {
				//System.out.printf("u fuked up mate: %s", e);
			}
		}
		return asdf;
	}

}