package opigrupp1;

public class KT1ul {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
						// indexid massiivis:    0,1,2,3,4,5,6 
		System.out.println(nullideArv( new int[]{1,3,0,5,0,6,0}) );
		
		// sama mis ülemine rida
		int[] massiiv = new int[]{1,3,0,5,0,6,0};
		int nulleMassiivis = nullideArv(massiiv);
		System.out.println(nulleMassiivis);
	}
	
	public static int nullideArv(int[] m){
		int nullideHulk = 0; // hoiab endas arvu kui palju on massiivis nulle
		
		for(int i = 0; i < m.length; i++){
			// kas massiivi m i element on null
			if(m[i] == 0){
				// kui on, liidame nullide hulgale ühe juurde
				nullideHulk++;
			}
		}
		// tagastan main meetodisse nullide hulga		
		return nullideHulk;
	}
	
	public static int nullideArv2(int[] m){
		int nullideHulk = 0;
		
		for(int mikihiir : m){
			//mikihiire sees on üks m masiivi sahtli sisu
			if(mikihiir == 0){
				nullideHulk++;
			}
		}
		
		return nullideHulk;
	}

}
